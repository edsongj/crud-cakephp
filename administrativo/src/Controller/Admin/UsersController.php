<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function index()
    {
        $this->paginate = [
            'limit' => 5
        ];
        $users = $this->paginate($this->Users);
        $this->set(compact('users'));
    }

    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }
    
    public function perfil()
    {
        $user = $this->Auth->user();

        $this->set(compact('user'));
    }

    public function editPerfil()
    {
        $user_id = $this->Auth->user('id');
        $user = $this->Users->get($user_id,
            [
                'contain' => []
            ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                // Para atualizar as informações do usuário para serem exibidas na perfil.ctp
                if($this->Auth->user('id') === $user->id){
                    $data = $user->toArray();
                    $this->Auth->setUser($data);
                }
                $this->Flash->success(__('Perfil editado com sucesso'));

                return $this->redirect(['controller' => 'Users', 'action' => 'perfil']);
            }
            $this->Flash->error(__('Erro: Perfil não editado com sucesso'));
        }
        // debug($user);
        $this->set(compact('user'));
    }

    public function editSenhaPerfil()
    {
        $user_id = $this->Auth->user('id');
        $user = $this->Users->get($user_id,
            [
                'contain' => []
            ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Senha editada com sucesso'));

                return $this->redirect(['controller' => 'Users', 'action' => 'perfil']);
            }
            $this->Flash->danger(__('Erro: Senha não editada com sucesso'));
        }
        // debug($user);
        $this->set(compact('user'));
    }
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                
                // Para utilizar o método Flash é necessário carregar em Controller/AppController.php
                    // As mensagens de sucesso e erro estão em src/Template/Element/Flash
                $this->Flash->success(__('Usuário cadastrado com Sucesso'));
                return $this->redirect(['action' => 'index']);

            }else{

                $this->Flash->danger(__('Erro: Usuário não cadastrado com sucesso'));
            }
        }
        $this->set(compact('user'));
    }

    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuário editado com sucesso'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Erro: Usuário não editado com sucesso'));
        }
        $this->set(compact('user'));
    }
    
    public function editSenha($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Senha do usuário editado com sucesso'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Erro: Senha do usuário não editado com sucesso'));
        }
        $this->set(compact('user'));
    }
    
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('Usuário excluído com sucesso'));
        } else {
            $this->Flash->danger(__('Erro: Usuário não excluído com sucesso'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function login()
    {
        //Para utilizar o método Auth é necessário carregar em Controller/AppController.php
        if ($this->request->is(['post'])) {
            $user = $this->Auth->identify();
            //debug($user);

            //Método criado em aula, envia qualquer usuário logado para página do index.ctp de welcome
            if($user){
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            
            // Caso o usuário não consiga logar, esse else traz para a página de login a mensagem de erro
            }else{
                $this->Flash->danger(__('Usuário ou Senha estão incorretos'));
            }

            /**
            * Método criado por mim para redirecionar o usuário definido como adm para entrar 
            * na página index.ctp de users que contém as ações de manipulação dos usuários no banco
            */
            // if($user){
            //    if($user['username'] == 'edson'){
            //         $this->Auth->setUser($user);
            //         return $this->redirect(['controller' => 'users', 'action' => 'index']);
            //     }else{
            //         $this->Auth->setUser($user);
            //         return $this->redirect($this->Auth->redirectUrl());
            //    }
            // }
        }
    }
    public function logout()
    {
        $this->Flash->success(__('Usuário deslogado com sucesso'));
        return $this->redirect($this->Auth->logout());
    }

    //Método para testar envio de valores por variável
    public function teste()
    {
        $t = 'Apenas um teste';
        $ts = ['um' => 1, 'dois' => 2];
        $this->set('t', $t);
        $this->set('ts', $ts);
    }
}
