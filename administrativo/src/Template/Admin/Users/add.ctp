<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Cadastrar Usuário</h2>
    </div>
    <a href="listar.html">
        <div class="p-2">
            <?= $this->Html->link(__('Listar'), ['controller' => 'users', 'action' => 'index'], ['class' => 'btn btn-outline-info btn-sm'])?>
        </div>
    </a>
</div><hr>
<?= $this->Flash->render();?>
<?= $this->Form->create($user);?>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for='name'><span class="text-danger">*</span> Nome</label>
            <?= $this->Form->control('name',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Nome completo', 'id' => 'name']);?>
        </div>
        <div class="form-group col-md-6">
            <!-- 
                * Para trabalhar a mensagem de erro ao validar o campo deve ser alterado em src/Model/Table/UsersTable.ctp 
                * Para personalizar a mensagem de erro deve atribuido a classe error-message no arquivo webroot/css/dashboard.css
            -->
            <label for='email'><span class="text-danger">*</span> E-mail</label>
            <?= $this->Form->control('email',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Seu melhor e-mail', 'id' => 'email']);?>
        </div>
    </div><div class="form-row">
        <div class="form-group col-md-6">
            <!-- Para trabalhar a mensagem de erro ao validar o campo deve ser alterado em src/Model/Table/UsersTable.ctp -->
            <label for='username'><span class="text-danger">*</span> Usuário</label>
            <?= $this->Form->control('username',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Usuário', 'id' => 'username']);?>
        </div>
        <div class="form-group col-md-6">
            <label for='pass'><span class="text-danger">*</span> Senha</label>
            <?= $this->Form->control('password',['label'=> false, 'class'=>'form-control', 'placeholder' => 'Informe Senha, mínimo 6 caracteres', 'id' => 'pass']);?>
        </div>
    </div>
    <p>
        <span class="text-danger">* </span>Campo obrigatório
    </p>
    <?= $this->Form->button(__('Cadastrar'), ['class'=>'btn btn-success'])?>
<?= $this->Form->end();?>
