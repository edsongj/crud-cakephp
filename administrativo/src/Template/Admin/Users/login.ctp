<?= $this->Form->create('post', ['class' => 'form-signin']); ?>
<?= $this->Html->image('logo_celke.png', ['class' => 'mb-4', 'alt' => 'Celke', 'width' => '72', 'height' => '72'] );?>
<h1 class="h3 mb-3 font-weight-normal">Área Restrita</h1>

<!-- Redenrizando mensagem de erro que está em Template/Element/Flash/danger.ctp -->
    <!-- Observação eliminar mensagem de retorno quando usuário deslogado tentar acessar alguma página -->
<?= $this->Flash->render(); ?>

<div class="form-group">
    <div style="text-align: left;">
        <label>Usuário</label>
    </div>
        <?= $this->Form->control('username', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Digite o usuário']);?>
</div>
<div class="form-group">
    <div style="text-align: left;">
        <label>Senha</label>
    </div>
    <?= $this->Form->control('password', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Digite a senha']);?>
</div>
<?= $this->Form->button(__('Entrar'), ['class' => 'btn btn-lg btn-primary btn-block']) ?>
<p class="text-center">Esqueceu a senha?</p>
<?= $this->Form->end() ?>