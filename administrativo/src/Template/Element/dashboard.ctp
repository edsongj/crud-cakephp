<nav class="sidebar">
    <ul class="list-unstyled">
        <li>
            <!-- Criando dashbord - Link da página Welcome -->
            <?= $this->Html->link(
                '<i class="fas fa-tachometer-alt"></i> Dashboard',
                [
                    'controller' => 'welcome',
                    'action' => 'index'
                ],
                [
                    // Para ignorar o html, caso contrário aparecerá o código do html e não o ícone
                    'escape' => false
                ]

            );?>
        </li>
        <li>
            <!-- Criando dashbord - Link da página Listar-->
            <?= $this->Html->link(
                '<i class="fas fa-users"></i> Usuários',
                [
                    'controller' => 'users',
                    'action' => 'index'
                ],
                [
                    // Para ignorar o html, caso contrário aparecerá o código do html e não o ícone
                    'escape' => false
                ]

            );?>
        </li>
        <li>
            <!-- Criando dashbord - Link do método Logout-->
            <?= $this->Html->link(
                '<i class="fas fa-sign-out-alt"></i> Sair',
                [
                    'controller' => 'users',
                    'action' => 'logout'
                ],
                [
                    // Para ignorar o html, caso contrário aparecerá o código do html e não o ícone
                    'escape' => false
                ]
            );?>
        </li>
        <a href="#"></a></li>
    </ul>
</nav>