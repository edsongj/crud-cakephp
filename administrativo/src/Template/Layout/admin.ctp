<!--
    # Esta página é responsável pelo layout do sistema 
    # No cabeçalho do html foi carregado os arquivos:
    * CSS:
        - bootstrap.min.css, fontawesome.min.css, dashboard.css
    * JS: 
        - fontawesome-all.min.js
    
    # No final do corpo do html foi carregado os arquivos:
    * JS:
        - jquery-3.4.1.min.js, popper.min.js, bootstrap.min.js, dashboard.js

    # Para utilizar o layout do admin deve configurar no AppController
-->
<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'Administrativo';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css(['bootstrap.min', 'fontawesome.min', 'dashboard']);?>
    <?= $this->Html->script(['fontawesome-all.min']);?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>

    <!-- Este cabeçalho está em  src/Template/Element-->
    <?= $this->element('cabecalho');?>

    <!-- Esta área é destinada ao container global da página, que contém todos os elementos e conteúdos -->
    <div class="d-flex">
        
        <!-- Este menu está em  src/Template/Element-->
        <?= $this->element('dashboard');?>

        <!-- Esta área é destinada ao conteúdo dinâmico das páginas do diretório Template/Admin/Users -->
        <div class="content p-1">
            <div class="list-group-item">
                
                <!-- fetch('content') responsável por carregar as páginas do diretório Template/Admin/Users -->
                <?= $this->fetch('content');?>
            </div>
        </div>
    </div>

    <?= $this->Html->script(['jquery-3.4.1.min', 'popper.min', 'bootstrap.min', 'dashboard']);?>
</body>
</html>
