<?php
    /**
     *Criando UsersController para determinar qual página deve ser carregada
     *Se deve carregar informações do banco de dados 
     *E qual view deve ser chamada
     */
    
    //Necessário atribuir um NAMESPACE para indicar o diretório Controller
        //App é o apelido para o src que contém o diretório Controller
    namespace App\Controller;

    //Inserindo o appcontroller
    use App\Controller\AppController;
    
    //Inserindo a classe UsersController para inserir os métodos que o sistema irá requerer
    class UsersController extends AppController
    {
        //Método será chamado para exibir a página inicial do projeto
        public function index()
        {
            /**
             *Enviando dados para a UsersController
             *$usuario = 'Edson'; //Descomente o trecho para a variável usuario ser atribuida
             *Instacia o set para inserir os dados da variável usuario para ser enviada para a view
             *$this->set(['usuarios' => $usuario]); //Descomente o trecho p/ enviar os dados p/ view
             */


            /**
             *Instancia a classe Users insere o método find para 
             *pesquisar e o atributo all para trazer todo os dados 
             */
            
            
             //Trazendo resultado do banco
            
            //Forma padrão
            //$usuarios = $this->Users->find()->all(); 

            //Com Pagination
                //Instanciando o método paginate
            $usuarios = $this->paginate($this->Users); 
            
            //$this->set(['usuarios' => $usuarios]); //Forma padrão para enviar os dados
            
            //Variavel usuarios será enviada para index.ctp
            $this->set(compact('usuarios'));

        }

        //Implementando o método de paginação de forma global
        public $paginate = [
            'limit' => 10,
            'order' => [
                //Apelido Users seguido do campo ID a ser ordenado
                'Users.id' => 'desc'
            ]
        ];

        //Implementando o método de visualizar os detalhes do usuário
        public function view($id = null) 
            /**
             * $id será passado via URL, por padrão está recebendo valor 
             * nulo para não impedir o funcionamento do sistema e ser atribuído quando o método for chamado
             */
        {
            //Variável usuario está recebendo os valores da tabela users e se tornará um array
                //Instancia usuario que recebe os valores correspondentes ao $id passado pela url
            $usuario = $this->Users->get($id);//retorna somente os dados de um usuario

            //Por ser um array deve ser enviado para a view.ctp como um array
            $this->set(['usuario' => $usuario]);
        }

        //Implementando o método de criação de novos usuários
        public function add()
        {
            //$user recebe da model Users os dados da nova entidade para serem manipulados
            $user = $this->Users->newEntity();

            //Validando os dados antes de salvar no banco
                /**
                 * Requisição é um post se sim valide os dados recebidos do formulário através do getData 
                 * valide através do patchEntity
                 */ 
            if($this->request->is('post')){
                $user = $this->Users->patchEntity($user, $this->request->getData());
                // utilizar debug para verificar os dados do formulário
                // debug($user);
                // exit;

                //Salvando os dados do usuário
                if($this->Users->save($user)){
                    $this->Flash->success(__('Usuário cadastrado com sucesso'));
                    return $this->redirect(['action' => 'index']);
                }else{
                    $this->Flash->error(__('Erro: Usuário não cadastrado com sucesso'));
                }
            }

            //Enviando os dados para add.ctp
            $this->set(compact($user));
        }

        //Implementando método para editar os dados do usuário
        public function edit($id = null)
            /**
             * $id será passado via URL, por padrão está recebendo valor 
             * nulo para não impedir o funcionamento do sistema e ser atribuído quando o método for chamado
             */
       
        {
            //Captura o id do usuário com o método get
            $user = $this->Users->get($id);

            //Validando os dados do fomulário para salvar no banco
                //Verificando se a requisição feita pelo usuário é um post, put ou get
                if($this->request->is(['post', 'put'])){
                    //Validando os dados com o patchEntity de $user
                        //Referenciando a model Users
                        //Após obter os dados do formulário os valores são atribuidos a variável user
                    $user = $this->Users->patchEntity($user, 
                    
                    //recebendo os dados do formulário
                    $this->request->getData());

                    /**
                     * Após validar os dados com o patchEntity, os dados serão salvos no banco com o 
                     * método save passando como parametro a variável user
                     */ 
                    //Enviando uma mensagem de sucesso ou de erro
                    if($this->Users->save($user)){
                        $this->Flash->success(__('Usuário editado com sucesso'));
                        return $this->redirect(['action' => 'index']);
                    }else{
                        $this->Flash->error(__('Erro: Usuário não editado com sucesso'));
                    }

                }
            //Enviando valor obtido para a edit.ctp
            $this->set(compact('user'));
        }

        //Implementando método para apagar usuário do banco
        public function delete($id = null)
        {
            //Método allowMethod permite apenas o recebimento do id pelo método post e o método delete do sql
            $this->request->allowMethod(['post', 'delete']);
            
            $user = $this->Users->get($id);
            //Caso o delete funcione corretamente o usuário será excluído do banco
            if($this->Users->delete($user)){

                //Mensagem de sucesso informando que o usuário foi excluído
                $this->Flash->success(('Usuário excluído com sucesso'));
            }else{

                //Mensagem de erro informando que o usuário não foi excluído
                $this->Flash->error(('Erro: Usuário não excluído com sucesso'));
            }

            //Após operação do delete ocorre o redirecionamento para a página index
            return $this->redirect(['action' => 'index']);
        }
    }

?>