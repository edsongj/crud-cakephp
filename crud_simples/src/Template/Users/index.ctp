<?php
    //Valor contido na variável usuarios está vindo da controller: UsersController
    // echo "<pre>";
    // var_dump($usuarios);
    // echo "</pre>";
?>
<div class='users index large-12 medium-12 comluns content'>
    <h3><?= 'Lista de Usuários';?></h3>
    <p style="text-align: right">
        <?= $this->Html->link(('Novo Usuário'),['action' => 'add'])?>
    </p>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php
                /**
                 * A Variável usuarios agora contém um array e para exibir os dados desse array 
                 * utiliza-se um foreach 
                 */
                foreach($usuarios as $usuario):
            ?>
                <tr>
                    <td><?= $usuario->id;?></td>
                    <td><?= $usuario->name;?></td>
                    <td><?= $usuario->email;?></td>
                    <td>
                        <?= 
                            /**
                             *Action redireciona para o método view
                             *$usuario->id informa o id p/ ser passado como parâmetro p/ o método view
                             */
                            $this->Html->link(('Ver'), ['action' => 'view', $usuario->id])?>
                            <?= $this->Html->link(('Editar'),['action' => 'edit', $usuario->id])?>
                            <?= $this->Form->postLink(('Apagar'),['action' => 'delete', $usuario->id], ['confirm' => 'Realmente deseja apagar o usuário?', $usuario->id]);?>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    <div class='paginator'>
        <ul class='pagination'>
            <?= $this->Paginator->first('<< '.__('Primeira'));?>
            <?= $this->Paginator->prev('< '.__('Anterior'));?>
            <?= $this->Paginator->numbers();?>
            <?= $this->Paginator->next('Próxima'. __(' >'));?>
            <?= $this->Paginator->last('Última'.__(' >>'));?>
        </ul>
        <p style="text-align: left">
            <?= $this->Paginator->counter(['format' => __('Página{{page}} de {{pages}}, mostrados {{current}} registro(s) do total de {{count}}')])?>
        </p>
    </div>
</div>