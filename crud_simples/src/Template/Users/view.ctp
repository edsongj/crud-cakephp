<div class="users view large-12 medium-12 columns content">
    <h3>Detalhe do Usuário</h3>
    <h3 style="color: darkcyan">Usuário: <?= $usuario->name?></h3>
    <table class="vertical-table">
        <tr>
            <th>ID</th>
            <td><?= $usuario->id?></td>
        </tr>
        <tr>
            <th>Name</th>
            <td><?= $usuario->name?></td>
        </tr>
        <tr>
            <th>Email</th>
            <td><?= $usuario->email?></td>
        </tr>
        <tr>
            <th>Usuário</th>
            <td><?= $usuario->username?></td>
        </tr>
        <tr>
            <th>Senha</th>
            <td><?= $usuario->password?></td>
        </tr>
        <tr>
            <th>Data de Cadastro</th>
            <td><?= $usuario->created?></td>
        </tr>
        <tr>
            <th>Última Alteração</th>
            <td><?= $usuario->modified?></td>
        </tr>
    </table>
    <?= $this->Html->link(('Listar Usuarios'),['controller' => 'users', 'action' => 'index'])?>
</div>