<h3>Cadstrar Usuário</h3>
<!--
    Será criado um formulário para preencher os dados
     do novo usuário para ser cadastrado no banco de dados
-->
<?= $this->Form->create();?>
    <label for="n">Nome <span style="color: red">*</span></label>
    <?= $this->Form->control('name', ['label' => false, 'id' => 'n', 'required']);?>

    <label for="e">Email <span style="color: red">*</span></label>
    <?= $this->Form->control('email', ['label' => false, 'id' => 'e', 'required']);?>

    <label for="u">Username <span style="color: red">*</span></label>
    <?= $this->Form->control('username', ['label' => false, 'id' => 'u', 'required']);?>

    <label for="p">Senha <span style="color: red">*</span></label>
    <?= $this->Form->control('password', ['label' => false, 'id' => 'p', 'required']);?>
    <p>
        Campo obrigatório <span style="color: red">*</span>
    </p>
    <?= $this->Form->button('Cadastrar');?>
<?= $this->Form->end();?>