<?php echo "<pre>"; print_r($user['password']); echo"</pre>";?>
<div class="users view large-12 medium-12 columns content">
    <h3>Editar Usuário</h3>
    <?= $this->Form->create($user)?>
        <?= $this->Form->control('name', ['label' => false, 'id' => 'n', 'required']);?>

        <label for="e">Email <span style="color: red">*</span></label>
        <?= $this->Form->control('email', ['label' => false, 'id' => 'e', 'required']);?>

        <label for="u">Username <span style="color: red">*</span></label>
        <?= $this->Form->control('username', ['label' => false, 'id' => 'u', 'required']);?>

        <label for="p">Senha <span style="color: red">*</span></label>
        <?= $this->Form->control('password', ['label' => false, 'id' => 'p', 'required']);?>
        <p>
            Campo obrigatório <span style="color: red">*</span>
        </p>
        <?= $this->Form->button('Salvar')?>
    <?= $this->Form->end()?>
</div>