<?php
    //Necessário atribuir um NAMESPACE para indicar o diretório Table
        //App é o apelido para o src que contém o diretório Model
    namespace App\Model\Table;

    //ORM\Query: Fornecendo uma interface simples para consulta ao banco
    use Cake\ORM\Query;

    //Responsável para verificar as regras
        //Antes dos dados serem salvos serão validados pelo RulesChecker
    use Cake\ORM\RulesChecker;

    //Provê acesso as coleções das entidades armazenadas em uma tabela específica
    use Cake\ORM\Table;

    //Validação dos dados
    use Cake\Validation\Validator;

    class UsersTable extends Table
    {
        public function initialize(array $config)
        {
            parent::initialize($config);
            $this->setTable('users');

            $this->addBehavior('Timestamp');
        }

        //Para impedir dados nulos para cadastrar novos usuários
        public function validationDefault(Validator $validator)
        {
            $validator
                    ->integer('id')
                    ->allowEmpty('id', 'create');
            
            $validator
                    ->requirePresence('name', 'create')
                    ->notEmpty('name');
            $validator
                    ->requirePresence('email', 'create')
                    ->notEmpty('email');
            $validator
                    ->requirePresence('username', 'create')
                    ->notEmpty('username');
            $validator
                    ->requirePresence('password', 'create')
                    ->notEmpty('password');
            return $validator;

        }

        //Implementando métedo para validar email e username como únicos no banco
        public function buildRules(RulesChecker $rules)
        {
            $rules->add($rules->isUnique(['email'], 'Este email já está em uso'));
            $rules->add($rules->isUnique(['username'], 'Este usuário já está em uso'));
            return $rules;
        }
    }

?>