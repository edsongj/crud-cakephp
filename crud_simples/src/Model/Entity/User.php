<?php
    /**
     * Criando User.php para trabalhar o método que busca os dados dos usuário no banco de dados
     */
    
    //Necessário atribuir um NAMESPACE para indicar o diretório Entity
        //App é o apelido para o src que contém o diretório Controller
    namespace App\Model\Entity;

    //ORM ou MOR: Mapeamento Objeto-Relacional
    use Cake\ORM\Entity;

    //Criptografar senha
    use Cake\Auth\DefaultPasswordHasher;

    //Criando classe User para buscar os dados do banco
    class User extends Entity
    {
        public $_accessible = [
            'id' => true,
            'name' => true,
            'email' => true,
            'username' => true,
            'password' => true,
            'created' => true,
            'modified' => true
        ];

        protected function _setPassword($password)
        {
            if (strlen($password) > 5) {
                return (new DefaultPasswordHasher)->hash($password);
            }
        }
    }

?>